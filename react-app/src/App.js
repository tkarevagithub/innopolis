//import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
//import logo from './logo.svg';
//import './App.css';
import PageProduct from './components/PageProduct/PageProduct';
import PageMain from './components/PageMain/PageMain';

function App() {
  return (
    //<div className="App">
      <BrowserRouter>
        <Routes>
           <Route path="/" element={<PageMain />} />
           <Route path="/product" element={<PageProduct />} />
        </Routes>
      </BrowserRouter>
    //</div>
  );
}

//const root = ReactDOM.createRoot(document.getElementById('root'));
//root.render(<App />); 

export default App;



