import './ColorButton.css';


function ColorButton(props) {
    const { item, actived } = props;
    const className = `product-color__img ${actived ? 'selected' : ''}`;

    return (
        <div className={className}>
            <img className="img-button" src={item.src} alt="Color of product" key={item.src}/>
        </div>
    );
}

export default ColorButton;
