import { useState } from 'react';
import './ProductColor.css';
import ColorButton from './ColorButton';




function ProductColor(props) {

    const { button } = props;
    const [activedColor, setActivedColor] = useState(0);
    
    return (
        <div>
            <section className="product-color">
                <div className="product-color__name">
                    <h3 className="h3">Цвет товара: синий</h3>
                </div>
                <div className="product-color__images">
                    {
                        button.map( (item, index) => {
                            return (
                                <div onClick={() => setActivedColor(item.src)} key={index}>
                                <ColorButton
                                    item={item}
                                    actived={item.src === activedColor}
                                />
                            </div>
                            )
                        })
                        }
                </div>
            </section>           
        </div>
    );
}

export default ProductColor;






