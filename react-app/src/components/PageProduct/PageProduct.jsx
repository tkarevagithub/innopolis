import './PageProduct.css';
import Header from '../Header/Header';
import Navigation from '../Navigation/Navigation';
import Main from '../Main/Main';
import Footer from '../Footer/Footer';


let navigationList = [
    {
      text: 'Электроника',
      link: '#',
    },
    {
      text: 'Смартфоны и гаджеты',
      link: '#',
    },
    {
      text: 'Мобильные телефоны',
      link: '#'
    },
    {
      text: 'Apple',
      link: '#'
    },
  ];

const footerLeftItems = [
    {
      title: 'Для уточнения информации звоните по номеру',
      text: '+7 900 000 0000',
      link: '/tel:79000000000',
    },
    {
      title: 'а предложения по сотрудничеству отправляйте на почту',
      text: 'partner@mymarket.com',
      link: '/mailto:partner@mymarket.com',
    },
];

const footerRightItems = [
  {
    text: 'Наверх',
    link: '/#Top',
  },
];

const footerItems = {left: footerLeftItems, right: footerRightItems};

function PageProduct() {
    return (
        <div>
            <Header />
            <div className="container">
                    <Navigation list={navigationList} />
                    <Main />
            </div>
            <Footer item={footerItems} />
        </div>
    )
}

export default PageProduct;
