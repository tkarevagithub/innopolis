import './Footer.css';
import Link from '../Link/Link';
import {useCurrentDate} from '@kundinos/react-hooks';



function Footer({item}) {
    const currentDate = useCurrentDate();
    const fullYear = currentDate.getFullYear();
    
    return (
        <div>
            <footer className="footer-background">
                <div className="footer-margin">
                    <div className="footer">
                        <div className="footer__left"> 
                            <strong>© ООО «<span className="heading__text_color">Мой</span>Маркет», 2018-{`${fullYear}`}</strong><br />
                            {
                                item.left.map(function ({ title, text, link }, index) {
                                        return (
                                            <div className="footer-item" key={index}>
                                                {title}<Link text={text} link={link} index={index} />,<br />
                                            </div>
                                        )   
                                })
                            }
                        </div>
                        <div className="footer__right">
                            {
                                item.right.map(function ({ text, link }, index) {
                                        return (
                                            <div className="footer-link-up" key={index}>
                                                <Link text={text} link={link} index={index} /><br />
                                            </div>
                                        )
                                })
                            }

                        </div>
                    </div> 

                </div>
            </footer>
        </div>
    );
}

export default Footer;


