import './Link.css';

function Link(props) {
  let { text, link} = props;

  return (
    <div className="a">
      <a className="a" rel="noreferrer" href={link}>{text}</a>
    </div>
  );
}

export default Link;
