import './ConfigurationButton.css';


function ConfigurationButton(props) {
    const { item, actived } = props;
    const className = `product-configuration__button ${actived ? 'selected' : ''}`;

    return (
        <div>
            <button className={className}>{item.text}</button>
        </div>
    );
}
export default ConfigurationButton;