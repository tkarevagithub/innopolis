import { useState } from 'react';
import './ProductConfiguration.css';
import ConfigurationButton from './ConfigurationButton';


function ProductConfiguration(props) {
    const {button} = props;
    const [activedColor, setActivedColor] = useState(0);

    return (
        <div>
            <section className="product-configuration">
                <div className="product-configuration__name">
                    <h3 className="h3">Конфигурация памяти: 128 ГБ</h3>
                </div>
                <div className="product-configuration__buttons">
                {
                        button.map( (item, index) => {
                            return (
                                <div onClick={() => setActivedColor(item.text)} key={index}>
                                <ConfigurationButton
                                    item={item}
                                    actived={item.text === activedColor}
                                />
                                </div>
                            )

                        })
                }
    
                </div>
            </section>
        </div>
    );
}

export default ProductConfiguration;





