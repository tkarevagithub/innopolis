import { createSlice } from "@reduxjs/toolkit";

function getItems(itemName) {
  let items;
  try {
    items = JSON.parse(localStorage.getItem(itemName)) || [];
  } catch {
    items = [];
  }
  return items;

}

export const cartSlice = createSlice({
  name: "cart",

  // Начальное состояние хранилища, товаров нет
  initialState: {
    products: getItems('products').length,
    likes: getItems('likes').length ,

},

  // Все доступные метода
  reducers: {
    // Добавить товар, первый параметр это текущее состояние
    // А второй параметр имеет данные для действия
    addProduct: (prevState, action) => {

      const cart = getItems('products');
      const indexOfStored = cart.indexOf(action.payload);
      if (indexOfStored === -1) {
        cart.push(action.payload);
        localStorage.setItem('products', JSON.stringify(cart));
      } else {
        cart.splice(indexOfStored, 1);
        localStorage.setItem('products', JSON.stringify(cart));
      }
        return {
            products: cart.length,
            likes: prevState.likes
        };
    },
    // removeProduct: (prevState) => {
    //     return prevState;
    // }
    addLike: (prevState, action) => {

      const likes = getItems('likes');
      const indexOfLiked = likes.indexOf(action.payload);
      if (indexOfLiked === -1) {
        likes.push(action.payload);
        localStorage.setItem('likes', JSON.stringify(likes));
      } else {
        likes.splice(indexOfLiked, 1);
        localStorage.setItem('likes', JSON.stringify(likes));
      }
        return {
            products: prevState.products,
            likes: likes.length
        };
    },
  },
});

// Экспортируем наружу все действия
export const { addProduct, addLike } = cartSlice.actions;

// И сам редуктор тоже
export default cartSlice.reducer;
