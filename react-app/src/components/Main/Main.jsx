import './Main.css';
import ProductViews from '../ProductViews/ProductViews';
import ProductColor from '../ProductColor/ProductColor';
import ProductConfiguration from '../ProductConfiguration/ProductConfiguration';
import ProductCharacteristics from '../ProductCharacteristics/ProductCharacteristics';
import ProductDescription from '../ProductDescription/ProductDescription';
import ProductComparison from '../ProductComparison/ProductComparison';
import Reviews from '../Reviews/Reviews';
import Form from '../Form/Form';
import Sidebar from '../Sidebar/Sidebar';


let characteristicitem = [
    {
      text: 'Apple A15 Bionic',
      link: 'https://ru.wikipedia.org/wiki/Apple_A15',
    },
];

let colorButtons = [
    {
        src: './images/color-1.png',
        className: "product-color__img",
    },
    {
        src: './images/color-2.png',
        className: "product-color__img",
    },
    {
        src: './images/color-3.png',
        className: "product-color__img",
    },
    {
        src: './images/color-4.png',
        className: "product-color__img_selected",
    },
    {
        src: './images/color-5.png',
        className: "product-color__img",
    },
    {
        src: './images/color-6.png',
        className: "product-color__img",
    },
];

let configurationButtons = [
    {
        text: '128 ГБ',
        className: "product-configuration__button_selected",
    },
    {
        text: '256 ГБ',
        className: "product-configuration__button",
    },
    {
        text: '512 ГБ',
        className: "product-configuration__button",
    },
];

let reviewItems = [
    {
        name: 'Марк Г.',
        src: './Mark.svg',
        experience: 'менее месяца',
        pluses: `это мой первый айфон после огромного количества телефонов на андроиде. всё
        плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,
        ширик тоже на высоте.`,
        minuses: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская
        вещь) а если нужно переносить фото с компа, то это только через itunes, который
        урезает качество фотографий исходное`
    },
    {
        name: 'Валерий Коваленко',
        src: './Valery.svg',
        experience: 'менее месяца',
        pluses: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
        minuses: `Плохая ремонтопригодность`
    },
];

let raiting = [
    ['star1', 'star1', 'star1', 'star1', 'star1'], 
    ['star2', 'star1', 'star1', 'star1', 'star1']
];

function Main() {
    
    return (
        <div>

            <ProductViews />

            <div className="product-container">
                <div className="product-information">

                    <ProductColor button = {colorButtons}/>
                    <ProductConfiguration button = {configurationButtons}/>
                    <ProductCharacteristics item = {characteristicitem}/>
                    <ProductDescription />
                    <ProductComparison />

                    <section className="reviews-container">

                        <Reviews item = {reviewItems} raiting = {raiting} />
                       
                        <Form />

                    </section>
                </div>

                <Sidebar />
                

            </div>
        </div>
    );
}

export default Main;