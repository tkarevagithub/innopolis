import './ProductViews.css';
import imageView1 from './image-1.webp';
import imageView2 from './image-2.webp';
import imageView3 from './image-3.webp';
import imageView4 from './image-4.webp';
import imageView5 from './image-5.webp';


function ProductViews() {
    return (
        <div>
            <h2 className="h2">
                Смартфон Apple iPhone 13 синий
            </h2>
            <section className="images-views">
                <img className="images-views__photo" src={imageView1} alt="View of product" />
                <img className="images-views__photo" src={imageView2} alt="View of product" />
                <img className="images-views__photo" src={imageView3} alt="View of product" />
                <img className="images-views__photo" src={imageView4} alt="View of product" />
                <img className="images-views__photo" src={imageView5} alt="View of product" />
            </section>
        </div>
    );
}

export default ProductViews;






