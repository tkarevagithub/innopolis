import './Navigation.css';
import Link from '../Link/Link';


function Navigation(props) {

    let { list } = props;
    return (
        <nav>
            <div className="navigation">
                {
                    list.map(function ({ text, link }, index) {

                        if (index) {

                            return (

                                <div className="navigation-item" key={index}>
                                    <div className="a">{'>'}</div>
                                    <Link text={text} link={link} index={index} />
                                </div>
                            )
                        } else {
                            return (

                                <div className="navigation-item" key={index}>
                                    <Link text={text} link={link} index={index} />
                                </div>
                            )
                        }
                    })
                }
            </div>
        </nav>
    );
}

export default Navigation;