import { useSelector, useDispatch} from 'react-redux';
import { addProduct, addLike } from '../Reducers/CartReducer'; 
//import { useState} from 'react';
import './Sidebar.css';
import Iframe from '../Iframe/Iframe';

function Sidebar() {
  const cart = useSelector((store) => store.cart);
  const dispatch = useDispatch();
//   const [/*product*/, setProduct] = useState(0)


  const handleClickProduct = () => {
    dispatch(addProduct('iphone'));
    
  };
  const handleClickLike = () => {
    dispatch(addLike('iphone'));
  };


    return (
        <div>
            <aside className="sidebar">
                <div className="sidebar-info">
                    <div className="sidebar-info__content">
                        <div className="sidebar-info__price">
                            <div className="sidebar-info__sale">
                                <div className="sidebar-info__container">
                                    <div className="old-price">
                                        75 990₽
                                    </div>
                                    <div className="sale">
                                        -8%
                                    </div>
                                </div>
                                <div onClick={handleClickLike}
                                     className={`sidebar-info__like ${
                                     cart.likes ? "like_choise" : ""}`}>
                                </div>
                            </div>
                            <div className="new-price">67 990₽</div>
                        </div>
                        <div className="delivery">
                            <div className="delivery__text">
                                Самовывоз в четверг, 1 сентября - <b>бесплатно</b>
                            </div>
                            <div className="delivery__text">
                                Курьером в четверг, 1 сентября - <b>бесплатно</b>
                            </div>
                        </div>
                    </div>

                    <button onClick={handleClickProduct}
                            className={` ${cart.products  ? 'choise' : 'sidebar__button'}`} >
                            {` ${cart.products  ? 'Убрать из корзины' : 'Добавить в корзину'}`}  
                    </button>
                </div>

                <div className="sidebar-iframe">
                    <div className="sidebar-iframe__name">
                        Реклама
                    </div>

                    <div className="sidebar-iframe__content">

                        <Iframe />
                        <Iframe />

                    </div>
                </div>
            </aside>
        </div>
    );
}

export default Sidebar;
