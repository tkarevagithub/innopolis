import React, {useState} from 'react';
import './Form.css';


function Form() {
      const [name, setName] = useState(() => {
        const nameInput = localStorage.getItem('name');
        return nameInput || "";
      });
      const [mark, setMark] = useState(() => {
        const markInput = localStorage.getItem('mark');
        return markInput || "";
      });
      const [text, setText] = useState(() => {
        const textInput = localStorage.getItem('text');
        return textInput || "";
      });
    
      const [errorName, setErrorName] = useState("");
      const [errorMark, setErrorMark] = useState("");
    
      const handleSubmit = (evt) => {
        evt.preventDefault();
        validateName();
        validateMark();
        clearForm();
      };
    
      let validName = false;
      let validMark = false;
      
      const validateMark = () => {
        if (mark >= 1 && mark <= 5) {
          validName = true;
        } else {
          setErrorMark("Оценка должна быть от 1 до 5");
        }
      };
      
      const validateName = () => {
        if (name.length === 0 || name === "") {
          setErrorName("Вы забыли указать имя и фамилию");
        } else if (name.length < 2) {
          setErrorName("Имя не может быть короче 2-х символов");
        } else {
          validMark = true;
        }
      };
    
      const clearForm = () => {
        if (validName === false && validMark === false) {
          setErrorMark("");
        }
        if (validName === true && validMark === true) {
          setName("");
          setMark("");
          setText("");
          localStorage.removeItem('name');
          localStorage.removeItem('mark');
          localStorage.removeItem('text');
        }
      };


    return (
        
            
      <div className="form">
      <div className="form__column_left"></div>
      <div className="form__column_right">
        <div>
          <h3>Добавить свой отзыв</h3>
        </div>
        <form className="form__form" method="POST" onSubmit={handleSubmit}>
          <div className="form__row_first">
            <div className="row-first-name">
            <input
              type="text"
              placeholder="Имя и фамилия"
              className='form__name'
              name="name"
              id="name"
              value={name}
              onChange={(evt) => {
                setName(evt.target.value);
                setErrorName("");
                localStorage.setItem('name', evt.target.value);
              }}
            />
            <div className={errorName ? "form__name_error" : 'form__name_noterror'}>
                {errorName}
              </div>
            </div>
            <div className="row-first-mark">
            <input
              name="mark"
              placeholder="Оценка"
              min="1"
              max="5"
              className='form__mark'
              id="mark"
              value={mark}
              type="numeric"
              onChange={(evt) => {
                setMark(evt.target.value);
                setErrorMark("");
                localStorage.setItem('mark', evt.target.value);
              }}
            />
            <div className={errorMark ? "form__mark_error" : 'form_mark_noterror'}>
                {errorMark}
              </div>
            </div>
            </div>
          
          <div className="form__row_second">
            <textarea
              name="text"
              placeholder="Текст отзыва"
              className="form__textarea"
              value={text}
              onChange={(evt) => {
                setText(evt.target.value);
                localStorage.setItem('text', evt.target.value);
              }}
            ></textarea>
          </div>
          <div className="form__btn">
            <button className="form__add-btn" type="submit">
              Отправить отзыв
            </button>
          </div>
        </form>
      </div>
    </div>


    );
}

export default Form;
