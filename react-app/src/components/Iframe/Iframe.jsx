import styled from 'styled-components';

const Content = styled.div`
  padding: 0px;
  margin: 0px;
  height: 300px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  background: #F0F0F0;
`;

function Iframe() {
  return (
    <Content>
       Здесь могла быть ваша реклама
    </Content>
    
        );
}

export default Iframe;