import { Link } from "react-router-dom";
//import './PageMain.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import styles from './PageMain.module.css';


const footerLeftItems = [
    {
      title: 'Для уточнения информации звоните по номеру',
      text: '+7 900 000 0000',
      link: '/tel:79000000000',
    },
    {
      title: 'а предложения по сотрудничеству отправляйте на почту',
      text: 'partner@mymarket.com',
      link: '/mailto:partner@mymarket.com',
    },
];

const footerRightItems = [
  {
    text: 'Наверх',
    link: '/#Top',
  },
];

const footerItems = {left: footerLeftItems, right: footerRightItems};


function PageMain() {
    return (
        <div>
            <Header />
            <div className={styles.mainpage}>
              <div className={styles.text}>
                Здесь должно быть содержимое главной страницы. <br />
                Но в рамках курса главная страница <br />
                используется лишь в демонстрационных целях
              </div>
                <Link to='/product' className={styles.link}>Перейти на страницу товара</Link>
            </div>
            <Footer item={footerItems} />
        </div>
    )
}

export default PageMain;
